const express = require('express');
const cors = require('cors');
const Config = require('./config/config');
const moviesController = require('./routes/moviesController');
const tvsController = require('./routes/tvsController');


const { port } = Config;
const app = express();

app.use(cors());
app.use(express.json());

app.use("/movies", moviesController);
app.use("/tvs", tvsController);

app.listen(port, () => console.log("Listening on port " + port));