const express = require('express');
const router = express.Router();
const axios = require('axios');
const {apiKey} = require('../config/config');
const apiUrl = "https://imdb-api.com/es/API/";

// Devolver todas las pelis con mallor puntuacion
router.get('/', (req,res,next)=>{
    axios.get(apiUrl+'Top250Movies/'+apiKey)
    .then(response => {
        res.json({ok: true, resp: response.data});
    })
    .catch(err => res.json({ok: false, resp: "Error con las peliculas"}));
});

// Devolver una peli
router.get('/one/:id', (req,res,next)=>{
    let id = req.params.id;
    axios.get(apiUrl+'Title/'+apiKey+'/'+id+'/Trailer,Ratings')
    .then(response => {
        res.json({ok: true, resp: response.data});
    })
    .catch(err => res.json({ok: false, resp: "Error con la peliculas."}));
});


// Devolver lista busqueda
router.get('/search/:word', (req,res,next) => {
    let search = req.params.word;
    axios.get(apiUrl+'SearchMovie/'+apiKey+'/'+search)
    .then(response => {
        res.json({ok: true, resp: response.data});
    })
    .catch(err => res.json({ok: false, resp: "Error con la busqueda de peliculas."}));
});

// Devolver rating de usuarios
router.get('/userRatings/:id', (req,res,next) => {
    let id = req.params.id;
    axios.get(apiUrl+'UserRatings/'+apiKey+'/'+id)
    .then(response => {
        res.json({ok: true, resp: response.data});
    })
    .catch(err => res.json({ok: false, resp: "Error con las peliculas recomendadas."}));
});

module.exports = router;