import './styles/main.css';
import { Container } from 'reactstrap';
import Header from './components/Header';
import MoviesMain from './components/movies/MoviesMain';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import MovieDetail from './components/movies/MovieDetail';
import { useState } from 'react';

const App = () => {

  const [searchWord, setSearchWord] = useState("");
  const [actualPage, setActualPage] = useState(1);

  const handleSearchWordChange = (word) => {
    setSearchWord(word);
    setActualPage(1);
  }

  return(
    <Router>
      <Container fluid>
        <Header searchWord={searchWord} setSearchWord={handleSearchWordChange} />
        <Switch>
          <Route exact path="/Detail/:id" component={MovieDetail} />
          <Route path="/" component={() => <MoviesMain actualPage={actualPage} searchWord={searchWord} />} />
        </Switch>
      </Container>    
    </Router>
  )
}

export default App;
