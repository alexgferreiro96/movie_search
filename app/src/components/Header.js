import { Col, Row, InputGroup, Input, Button  } from 'reactstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from '@fortawesome/free-solid-svg-icons';
import Sidebar from './Sidebar';
import MainLogo from '../img/logopng.png';
import { Link } from 'react-router-dom';

const Header = (props) => {
    return(
        <Row className="align-items-center bg-general justify-content-between">
            <Col md={1}>
                <Sidebar />
            </Col>
            <Col md={4}>
                <InputGroup>
                    <Input type="text" placeholder="Buscar películas por nombre..." value={props.searchWord} onChange={(e) => props.setSearchWord(e.target.value)} />
                </InputGroup>
            </Col>
            <Col sm={1} className="text-right">
                <Link to="/">
                    <img src={MainLogo} className="img-fluid" alt="MainLogo" />
                </Link>
            </Col>
        </Row>
    );
}

export default Header;