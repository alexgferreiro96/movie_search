import React, { useEffect, useState } from 'react';
import { Row, Col } from 'reactstrap';
import MoviesController from '../../controller/MoviesController';
import noImage from '../../img/no_available.jpg';
import { Link } from 'react-router-dom';
import PacmanLoader from "react-spinners/PacmanLoader";

const FooterMovieDetail = (props) => {

    const [movies,setMovies] = useState(null);

    useEffect(()=>{
        MoviesController.getSimilarMovies(props.id)
          .then(listMovies => {
              listMovies.resp.results.map(el=>{
                  if(el.poster_path === null) {
                    el.poster_path = noImage;
                  }else{ 
                    el.poster_path = "https://image.tmdb.org/t/p/w200"+ el.poster_path;
                  }
                  console.log(el);
                  return el;
              });
              listMovies.resp.results.splice(6);
              setMovies(listMovies.resp.results);
          })
          .catch(error => setMovies([]));
    }, [props]);

    const displayListMovies = !movies ? <p>No se han encontrado peliculas</p> : React.Children.toArray(movies.map((el, index) => {
        return (
                <Col sm={12} md={2} lg={2} className="d-flex justify-content-center">
                    <div className="movie-cover rounded w-75 h-75">
                        <Link to={"/Detail/"+el.id}>
                            <img src={el.poster_path} alt={"Image top card "+el.poster_path} className="img-fluid" />
                        </Link>
                    </div>
                </Col>
            );
    }));

    return(
        <div className="position-fixed footer p-3 bg-dark w-100">
            <h5 className="text-white">Titulos similares:</h5>
            <Row className="justify-content-center">
                {!movies ? 
                    <Col sm={2} className="mb-5">
                        <PacmanLoader color={'#FFFF00'} loading={true} size={25} />
                    </Col> 
                    : 
                    displayListMovies.length > 0 ? displayListMovies : <p className="text-white">No se han podido encontrar recomendaciones</p>
                }
            </Row>
        </div>
    );
}

export default FooterMovieDetail;
