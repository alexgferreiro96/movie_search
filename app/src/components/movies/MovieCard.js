import { Link } from 'react-router-dom';
import { CardImg, Col } from 'reactstrap';
const MovieCard = (props) => {
    const movie = props.movie;
    return(
        <Col sm={12} md={4} lg={3} className="p-3">
            <div className="h-100 movie-cover rounded p-2">
                <Link to={"/Detail/"+movie.id}>
                    <CardImg top src={movie.poster_path} alt="Image top card" />
                </Link>
            </div>
        </Col>
    );
}

export default MovieCard;
