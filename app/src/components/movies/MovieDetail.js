import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Col, Row, Container } from 'reactstrap';
import MoviesController from '../../controller/MoviesController';
import ReactStars from "react-rating-stars-component";
import PacmanLoader from "react-spinners/PacmanLoader";
import FooterMovieDetail from './FooterMovieDetail';

const MovieDetail = () => {

    const [movie, setMovie] = useState(false);
    const {id} = useParams();

    useEffect(()=>{
        MoviesController.getOneMovie(id)
        .then(movieDB => {
            movieDB.resp.poster_path = "https://image.tmdb.org/t/p/w200"+ movieDB.resp.poster_path;
            setMovie(movieDB.resp);
        })
        .catch(error => setMovie([]));
    }, [id]);

    return(
        <Container fluid>
            <Row className="justify-content-center p-3 align-items-center">
                <Col md={12} lg={9} xl={9}>
                    {movie?
                        <Row className="bg-general rounded p-3">
                            <Col sm={12} md={6} className="d-flex justify-content-center p-3">
                                <img src={movie.poster_path} alt={"Image-"+movie.id} className="rounded" />
                            </Col>
                            <Col sm={12} md={6} className="p-3 text-white">
                                <h2>{movie.title}</h2>
                                <div>
                                    <p className="text-justify">{movie.overview}</p>
                                    <p>
                                        Generos: {' '}
                                        {React.Children.toArray(
                                            movie && movie.genres.map((genre, index, arrGenres) => {
                                                if(index === arrGenres.length-1) return <span>{genre.name}</span>;
                                                return <span>{genre.name}, </span>;
                                            })
                                        )}
                                    </p>
                                    <ReactStars
                                        count={5}
                                        size={24}
                                        value={movie.vote_average/2}
                                        edit={false}
                                        isHalf={true}
                                        emptyIcon={<i className="far fa-star"></i>}
                                        halfIcon={<i className="fa fa-star-half-alt"></i>}
                                        fullIcon={<i className="fa fa-star"></i>}
                                        activeColor="#ffd700"
                                    />
                                </div>
                            </Col>
                        </Row>
                    : 
                        <Row className="justify-content-center mt-5">
                            <Col sm={3}>
                                <PacmanLoader color={'#FFFF00'} loading={true} size={50} />
                            </Col>
                        </Row>
                    }
                    
                </Col> 
            </Row>
            <FooterMovieDetail id={id} />
        </Container>
    );
}

export default MovieDetail;