import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';

const MoviesGenres = (props) => {

    const genres = props.genres;

    const displayGenres = genres.length === 0? null : React.Children.toArray(genres.map((genre,index) => {
        return <ListGroupItem active={genre.state} color="secondary" tag="button" action onClick={(e) => props.modifyGenreState(e.target.innerText)}>{genre.genreElement.name}</ListGroupItem>
    }));

    return(
        <ListGroup className="list-group list-group-flush rounded">
            {displayGenres}
        </ListGroup>
    );
}

export default MoviesGenres;