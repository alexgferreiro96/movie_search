import React from 'react';
import { Row } from 'reactstrap';
import MovieCard from './MovieCard';

const MoviesList = (props) => {

    const movies = props.movies;

    const displayListMovies = typeof movies === 'undefined' ? 
        <p>No se han encontrado peliculas</p> 
        : 
        React.Children.toArray(movies && movies.map((el, index) => {
            return <MovieCard movie={el} />;
    }));

    return(
        <Row>
            {displayListMovies}
        </Row>
    );
}

export default MoviesList;