import {  Col, Row } from 'reactstrap';
import { useState, useEffect } from 'react';
import MoviesList from './MoviesList';
import MoviesPagination from './MoviesPagination';
import noImage from '../../img/no_available.jpg';
import MoviesController from '../../controller/MoviesController';

const MoviesMain = (props) => {

    const searchWord = props.searchWord;
    const [movies, setMovies] = useState([]);
    const [pagination, setPagination] = useState();
    const [actualPage, setActualPage] = useState(1);

    useEffect(()=>{

        const getAll = () => {
          MoviesController.getAllMovies(actualPage)
          .then(listMovies => {
              listMovies.resp.results.map(el=>{
                  if(el.poster_path === null) {
                    el.poster_path = noImage;
                  }else{ 
                    el.poster_path = "https://image.tmdb.org/t/p/w200"+ el.poster_path;
                  }
                  return el;
              });
              setPagination(listMovies.resp);
              setMovies(listMovies.resp.results);
          })
          .catch(error => setMovies([]));
        }
  
        const getWord = (word, page) => {
            MoviesController.searchMoviesByWord(word,page)
            .then(listMovies => {
                listMovies.resp.results.map(el=>{
                  if(el.poster_path === null) {
                    el.poster_path = noImage;
                  }else{ 
                    el.poster_path = "https://image.tmdb.org/t/p/w200"+ el.poster_path;
                  }
                  return el;
              });
              setPagination(listMovies.resp);
              setMovies(listMovies.resp.results);
            })
            .catch(error => setMovies([]))
        }
  
        searchWord !== "" ? getWord(searchWord, actualPage) : getAll();
  
    }, [actualPage, setActualPage,searchWord,props.searchWord]);

    return(
        <Row className="justify-content-center p-3">
            <Col md={12} lg={8} xl={8} className="bg-general rounded pr-4 pl-4">
                <Row className="justify-content-center">
                    <MoviesList movies={movies} />
                </Row>
                <Row className="justify-content-center">
                    <MoviesPagination pagination={pagination} />
                </Row>
            </Col>
        </Row>
    );
}

export default MoviesMain;