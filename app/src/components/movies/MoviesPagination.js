import React from 'react';
import Pagination from 'react-js-pagination';

const MoviesPagination = (props) => {

    const pagination = props.pagination;

    const displayPagination = typeof pagination === 'undefined' ? null : (
        <Pagination
                activePage={pagination.page}
                itemsCountPerPage={pagination.results.length}
                totalItemsCount={pagination.total_results}
                onChange={(numberPage)=>props.setActualPage(numberPage)}
                itemClass="page-item"
                linkClass="page-link"
            />
    );

    return pagination && pagination.results.length < pagination.total_results ? displayPagination : null;
}

export default MoviesPagination;