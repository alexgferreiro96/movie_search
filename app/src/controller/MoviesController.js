const axios = require('axios');
const API_URL = "https://movie-search-agfworks.herokuapp.com/movies";

export default class MoviesController{
    static getAllMovies = (newPage) => {
        return new Promise((resolve, reject) => {
            axios.get(API_URL,{params:{page: newPage}})
            .then(list => {
                if(list.data.ok) resolve(list.data);
                throw new Error("Error en la consulta");
            })
            .catch(error => reject(error));
        })
    }

    static getOneMovie = (id) => {
        return new Promise((resolve, reject) => {
            axios.get(API_URL+'/one/'+id)
            .then(list => {
                if(list.data.ok) resolve(list.data);
                throw new Error("Error en la consulta");
            })
            .catch(error => reject(error));
        })
    }

    static searchMoviesByWord = (word, newPage) => {
        return new Promise((resolve, reject) => {
            axios.get(API_URL+'/search/'+word,{params:{page: newPage}})
            .then(list => {
                if(list.data.ok) resolve(list.data);
                throw new Error("Error en la consulta");
            })
            .catch(error => reject(error));
        })
    }

    static getSimilarMovies = (id) => {
        return new Promise((resolve, reject) => {
            axios.get(API_URL+'/sames/'+id)
            .then(list => {
                if(list.data.ok) resolve(list.data);
                throw new Error("Error en la consulta");
            })
            .catch(error => reject(error));
        })
    }

    static getAllCategoriesMovies = () => {
        return new Promise((resolve, reject) => {
            axios.get(API_URL+'/genres')
            .then(list => {
                if(list.data.ok) resolve(list.data);
                throw new Error("Error en la consulta");
            })
            .catch(error => reject(error));
        })
    }
}